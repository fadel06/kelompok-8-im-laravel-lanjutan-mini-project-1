<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GuruSeeder::class);
        $this->call(SiswaSeeder::class);
        $this->call(MapelSeeder::class);
    }
}
