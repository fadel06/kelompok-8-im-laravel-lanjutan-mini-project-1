<?php

use Illuminate\Database\Seeder;

class MapelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mapels = [
          [
            'kode_mapel' => 'B001',
            'nama_pelajaran' => 'Bahasa Indonesia'
          ],
          [
            'kode_mapel' => 'B002',
            'nama_pelajaran' => 'Bahasa Inggris'
          ],
          [
            'kode_mapel' => 'M001',
            'nama_pelajaran' => 'Matematika'
          ],
          [
            'kode_mapel' => 'S001',
            'nama_pelajaran' => 'Sejarah'
          ],
          [
            'kode_mapel' => 'F001',
            'nama_pelajaran' => 'Fisika'
          ],
        ];
        DB::table('mapel')->insert($mapels);
    }
}
