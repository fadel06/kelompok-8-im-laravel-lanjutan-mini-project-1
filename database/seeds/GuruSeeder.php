<?php

use Illuminate\Database\Seeder;

class GuruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gurus = [
          [
            'nip' => '202010001',
            'nama_depan' => 'Abdul',
            'nama_belakang' => 'Abdul',
            'jenis_kelamin' => 'L',
            'agama' => 'Islam',
            'alamat' => 'Jakarta'
          ],
          [
            'nip' => '202010002',
            'nama_depan' => 'Baim',
            'nama_belakang' => 'Baim',
            'jenis_kelamin' => 'L',
            'agama' => 'Islam',
            'alamat' => 'Jakarta'
          ],
          [
            'nip' => '202010003',
            'nama_depan' => 'Amanda',
            'nama_belakang' => 'Amanda',
            'jenis_kelamin' => 'P',
            'agama' => 'Non-Muslim',
            'alamat' => 'Jakarta'
          ],
          [
            'nip' => '202010004',
            'nama_depan' => 'Tasya',
            'nama_belakang' => 'Tasya',
            'jenis_kelamin' => 'P',
            'agama' => 'Muallaf',
            'alamat' => 'Jakarta'
          ]
        ];
        DB::table('guru')->insert($gurus);
    }
}
