<?php

use Illuminate\Database\Seeder;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $siswas = [
        [
          'nim' => 'D202010001',
          'nama_depan' => 'Latif',
          'nama_belakang' => 'Latif',
          'jenis_kelamin' => 'L',
          'agama' => 'Islam',
          'alamat' => 'Jakarta'
        ],
        [
          'nim' => 'D202010002',
          'nama_depan' => 'Deni',
          'nama_belakang' => 'Deni',
          'jenis_kelamin' => 'L',
          'agama' => 'Islam',
          'alamat' => 'Jakarta'
        ],
        [
          'nim' => 'D202010003',
          'nama_depan' => 'Lin',
          'nama_belakang' => 'Lin',
          'jenis_kelamin' => 'P',
          'agama' => 'Non-Muslim',
          'alamat' => 'Jakarta'
        ],
        [
          'nim' => 'D202010004',
          'nama_depan' => 'Sandra',
          'nama_belakang' => 'Sandra',
          'jenis_kelamin' => 'P',
          'agama' => 'Muallaf',
          'alamat' => 'Jakarta'
        ]
      ];
      DB::table('siswa')->insert($siswas);
    }
}
